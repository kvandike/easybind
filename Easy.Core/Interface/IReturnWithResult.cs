﻿using System;

namespace Easy.Core
{
	public interface IReturnWithResult
	{

		bool ReturnWithResult(Intent intent);
		
	}
}

