﻿using System;
using System.Collections.Generic;

namespace Easy.Core
{
	public interface IBroadcast
	{

		void SubscribeOnBroadcast();

		void UnscribeOnBroadcast();

		void Register<T>(Action<T> action, string token = null);

		void UnRegister<T>(string token = null);

		void Send<T>(T obj, string token = null);
	}
}

