﻿using System;
using System.Windows.Input;

namespace Easy.Core
{
	public class WeakRelayCommand : ICommand
	{
		WeakReference<Func<object, bool>> canExecute;
		readonly WeakReference<Action<object>> executeAction;

		public WeakRelayCommand(Action<object> executeAction)
			: this(executeAction, null)
		{
		}

		public WeakRelayCommand(Action<object> executeAction, Func<object, bool> canExecute)
		{
			if (executeAction == null)
			{
				throw new ArgumentNullException("executeAction");
			}
			this.executeAction =new WeakReference<Action<object>>(executeAction);
			this.canExecute =new WeakReference<Func<object, bool>>(canExecute);
		}

		public bool CanExecute(object parameter)
		{
			bool result = true;

			Func<object, bool> canExecuteHandler;
			if (canExecute.TryGetTarget (out canExecuteHandler)) {
				if (canExecuteHandler != null) {
					result = canExecuteHandler (parameter);
				}
				return result;
			}
			return result;
		}

		public event EventHandler CanExecuteChanged;

		public void RaiseCanExecuteChanged()
		{
			EventHandler handler = CanExecuteChanged;
			if (handler != null)
			{
				handler(this, new EventArgs());
			}
		}

		public void Execute(object parameter)
		{
			Action<object> action;
			if (executeAction.TryGetTarget (out action)) {
				action (parameter);
			}
		}
	}
}

