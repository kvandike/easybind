﻿namespace Easy.Core
{
	public abstract class BootstrapBase
	{
		readonly IIocContainer _iocContainer;

		static volatile BootstrapBase _locator;

		internal static BootstrapBase Locator {
			get {
				return _locator;
			}
		}

		public IIocContainer IocContainer {
			get {
				return _iocContainer;
			}
		}




		protected BootstrapBase (IIocContainer container)
		{
			_iocContainer = container;
			_locator = this;
		}
	}
}

