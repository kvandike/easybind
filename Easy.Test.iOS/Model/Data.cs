﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Easy.Core;

namespace Easy.Test.iOS
{
	public class Data : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion

		public DataSecond DataSecond { get; set;}

		string name;
		public string Name {
			get {
				return name;
			}
			set {
				name = value;
				OnPropertyChanged ();
			}
		}

		public RelayCommand Command{
			get{
				return new RelayCommand (s => {
					Name = "RelayCommand";
					Console.WriteLine("Relay click");
				});
			}
		}


		protected virtual void OnPropertyChanged ([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}

	public class DataSecond : INotifyPropertyChanged
	{
		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		#endregion


		string name;
		public string Name {
			get {
				return name;
			}
			set {
				name = value;
				OnPropertyChanged ();
			}
		}


		protected virtual void OnPropertyChanged ([CallerMemberName] string propertyName = null)
		{
			var handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(propertyName));
			}
		}
	}
}

