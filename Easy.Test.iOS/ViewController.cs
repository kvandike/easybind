﻿using System;

using UIKit;
using CoreGraphics;
using Easy.Core.Bind;

namespace Easy.Test.iOS
{
	public partial class ViewController : UIViewController
	{
		public ViewController (IntPtr handle) : base (handle)
		{
		}

		public ViewController(){
		}

		Data data;


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = UIColor.White;
			data = new Data ();
			data.DataSecond = new DataSecond ();
			data.DataSecond.Name="tete";
			data.Name = "1";
			var field = new UITextField (new CGRect (100, 50, View.Bounds.Width, 50));
			field.Text = data.Name;

			var button = new UIButton (UIButtonType.RoundedRect);
			button.SetTitle ("Clicked", UIControlState.Normal);
			button.Frame = new CGRect (100, 120, View.Bounds.Width, 50);
			var builder = new BindingBuilder ();
			var bind = builder
				.SetSource (data, () => data.Name)
				.SetTarget (() => field.Text)
				.SetBinding (BindingMode.TwoWay)
				.UpdateTargetTrigger ("EditingChanged")
				.Build ();

			var command = new ControlCommand (button, "TouchUpInside", data.Command);

			var unbutton = new UIButton (UIButtonType.RoundedRect);
			unbutton.SetTitle ("Unscribe", UIControlState.Normal);
			unbutton.Frame = new CGRect (100, 200, View.Bounds.Width, 50);
			unbutton.TouchUpInside += (s, e) => {
				command.UnSubscribe();
			};
//			data.PropertyChanged += (s, e) => {
//				button.SetTitle (data.Name, UIControlState.Normal);
//			};
			View.AddSubview (field);
			View.AddSubview (button);
			View.AddSubview (unbutton);
			// Perform any additional setup after loading the view, typically from a nib.
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}
	}
}

