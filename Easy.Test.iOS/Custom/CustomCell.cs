﻿using System;
using UIKit;
using CoreGraphics;

namespace Easy.Test.iOS
{
	public class CustomCell : UITableViewCell
	{
		UILabel label;

		string text;
		public string Text {
			get {
				return text;
			}
			set {
				text = value;
				label.Text = text;
			}
		}

		public CustomCell ()
		{
			label = new UILabel (new CGRect (15, 10, 100, 40));
			ContentView.AddSubview (label);
		}
	}
}

