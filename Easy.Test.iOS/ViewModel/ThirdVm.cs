﻿using Easy.Core;
using System.Windows.Input;
using Easy.Core.iOS;

namespace Easy.Test.iOS
{
	public class ThirdVm: ViewModelBase
	{
		ICommand goBackCommand;
		public ICommand GoBackCommand {
			get {
				return goBackCommand;
			}
			set {
				goBackCommand = value;
				OnPropertyChanged ();
			}
		}

		ICommand goBackWithResultCommand;
		public ICommand GoBackWithResultCommand {
			get {
				return goBackWithResultCommand;
			}
			set {
				goBackWithResultCommand = value;
				OnPropertyChanged ();
			}
		}

		ICommand pushGroupCommand;
		public ICommand PushGroupCommand {
			get {
				return pushGroupCommand;
			}
			set {
				pushGroupCommand = value;
				OnPropertyChanged ();
			}
		}

		public ThirdVm(){
			GoBackCommand = new RelayCommand (o => {
				var intent = CreateIntent();
				intent.GoBack ();
			});
			GoBackWithResultCommand = new RelayCommand (o => {
				var intent = CreateIntent();
				intent.Put ("data",new Data{ Name = "resultName"});
				intent.GoBackWithResult ();
			});
			PushGroupCommand = new RelayCommand (o => {
				var customParameters = Device.OnPlatform<string> (() => "toNav", () => "android", () => "windows");
				var intent = CreateIntent ();
				intent.Navigate (AppDelegate.FourthView,customParameters);
			});
			Broadcast.Send<Data> (new Data{Name = "gfgfg"},"token");
		}

	}
}

