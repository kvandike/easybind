﻿using System;
using Easy.Core;
using System.Windows.Input;
using Easy.Core.iOS;

namespace Easy.Test.iOS
{
	public class FirstVm : ViewModelBase
	{

	
		ICommand presentCommand;
		public ICommand PresentCommand {
			get {
				return presentCommand;
			}
			set {
				presentCommand = value;
				OnPropertyChanged ();
			}
		}

		ICommand pushCommand;
		public ICommand PushCommand {
			get {
				return pushCommand;
			}
			set {
				pushCommand = value;
				OnPropertyChanged ();
			}
		}

		public FirstVm(){
			PresentCommand = new RelayCommand (o => {
				var intent = CreateIntent();
				intent.Put ("first",true);

				intent.Navigate (AppDelegate.ThirdView,NavigationService.ToPresent);
			});
			PushCommand = new RelayCommand (o => {
				var intent = CreateIntent();
				intent.RemoveToNavigate = true;
				intent.Navigate (AppDelegate.SecondView);
			});
			Broadcast.SubscribeOnBroadcast ();
//			Broadcast.Register<Data> (s => {
//				int h =0;
//			},"token");
			Broadcast.Register<Data> (s => {
				int h =1;
			},"token");
			Broadcast.Register<Data> (Sender,"token");
			Broadcast.Register<Data> (SenderStatic,"token");

			int g = 0;
		}


		void Sender(Data data){
			int h =0;
		}

		static void SenderStatic(Data data){
			int h =0;
		}
	}

}

