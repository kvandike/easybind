﻿using System;
using Easy.Core;
using System.Windows.Input;

namespace Easy.Test.iOS
{
	public class SecondVm: ViewModelBase
	{
		ICommand goBackCommand;
		public ICommand GoBackCommand {
			get {
				return goBackCommand;
			}
			set {
				goBackCommand = value;
				OnPropertyChanged ();
			}
		}

		ICommand goBackWithResultCommand;
		public ICommand GoBackWithResultCommand {
			get {
				return goBackWithResultCommand;
			}
			set {
				goBackWithResultCommand = value;
				OnPropertyChanged ();
			}
		}

		public SecondVm(){
			GoBackCommand = new RelayCommand (o => {
				var intent = CreateIntent();
				intent.GoBack ();
			});
			GoBackWithResultCommand = new RelayCommand (o => {
				var intent = CreateIntent();
				intent.Put ("test",false);
				intent.GoBackWithResult ();
			});
		}
	}
}

