﻿using System;
using Easy.Core;

namespace Easy.Test.iOS
{
	public class FourthVm : ViewModelBase
	{
		GroupRoot _groupRoot;

		public GroupRoot GroupRoot {
			get {
				return _groupRoot;
			}
		}

		public FourthVm ()
		{
			_groupRoot = new GroupRoot ();

			var section1 = new GroupSection {Title = "Section1"};
			var section2 = new GroupSection {Title = "Section2"};
			var section3 = new GroupSection {Title = "Section3"};
			GroupCell cell2_2 = null;

			var cell1_1 = new GroupCell ();
			cell1_1.PrimaryText = "cell 1 _ 1";
			cell1_1.CellStyle =GroupCellStyle.Primary | GroupCellStyle.Disclosure | GroupCellStyle.RowClick;
			cell1_1.Command = new RelayCommand ((o) => {
				_groupRoot[1].RemoveAt (1);
				Console.WriteLine ("cell 1 _ 1");
			});

			var cell1_2 = new GroupCell ();
			cell1_2.PrimaryText = "cell 1 _ 2";
			cell1_2.CellStyle = GroupCellStyle.Primary | GroupCellStyle.Disclosure | GroupCellStyle.RowClick;
			cell1_2.Command = new RelayCommand ((o) => {
				_groupRoot[1].Insert (1,cell2_2);
				Console.WriteLine ("cell 1 _ 2");
			});
			var cell1_3 = new GroupCell ();
			cell1_3.PrimaryText = "cell 1 _ 3"; 
			cell1_3.CellStyle =GroupCellStyle.Primary | GroupCellStyle.Detail | GroupCellStyle.RowClick;
			cell1_3.Command = new RelayCommand ((o) => Console.WriteLine ("cell 1 _ 3"));
			cell1_3.AccessoryCommand = new RelayCommand (o => Console.WriteLine ("Detail Click"));
			section1.Add (cell1_1);
			section1.Add (cell1_2);
			section1.Add (cell1_3);



			var cell2_1 = new GroupCell ();
			cell2_1.PrimaryText = "cell 2 _ 1";
			cell2_1.SecondaryText = "cell secondary 2 _ 1";
			cell2_1.CellStyle =  GroupCellStyle.Secondary | GroupCellStyle.CheckMark;
			cell2_1.Command = new RelayCommand ((o) => Console.WriteLine ("cell 2 _ 1"));
			cell2_2 = new GroupCell ();
			cell2_2.PrimaryText = "cell 2 _ 2";
			cell2_2.SecondaryText = "cell secondary 2 _ 2";
			cell2_2.CellStyle = GroupCellStyle.Secondary | GroupCellStyle.CheckMark;
			cell2_2.Command = new RelayCommand ((o) => Console.WriteLine ("cell 2 _ 2"));
			var cell2_3 = new GroupCell ();
			cell2_3.PrimaryText = "cell 2 _ 3";
			cell2_3.SecondaryText = "cell secondary 2 _ 3";
			cell2_3.CellStyle = GroupCellStyle.Secondary | GroupCellStyle.CheckMark;
			cell2_3.Command = new RelayCommand ((o) => Console.WriteLine ("cell 2 _ 3"));
			section2.Add (cell2_1);
			section2.Add (cell2_2);
			section2.Add (cell2_3);


			var cell3_1 = new GroupCell ();
			cell3_1.PrimaryText = "cell 3 _ 1";
			cell3_1.SecondaryText = "cell secondary 3 _ 1";
			cell3_1.Data = new Data{Name = "cell 3 _ 1"};
			cell3_1.CellStyle = GroupCellStyle.Custom | GroupCellStyle.RowClick;
			cell3_1.Command = new RelayCommand ((o) => Console.WriteLine ("cell 3 _ 1"));
			var cell3_2 = new GroupCell ();
			cell3_2.PrimaryText = "cell 3 _ 2";
			cell3_2.SecondaryText = "cell secondary 3 _ 2";
			cell3_2.Data = new Data{Name = "cell 3 _ 2"};
			cell3_2.CellStyle = GroupCellStyle.Custom | GroupCellStyle.RowClick;
			cell3_2.Command = new RelayCommand ((o) => Console.WriteLine ("cell 3 _ 2"));
			var cell3_3 = new GroupCell ();
			cell3_3.PrimaryText = "cell 3 _ 3";
			cell3_3.SecondaryText = "cell secondary 3 _ 3";
			cell3_3.CellStyle =  GroupCellStyle.Custom | GroupCellStyle.RowClick;
			cell3_3.Data = new Data{Name = "cell 3 _ 3"};
			cell3_3.Command = new RelayCommand ((o) => Console.WriteLine ("cell 3 _ 3"));
			cell3_1.Height = 120;
			section3.Add (cell3_1);
			section3.Add (cell3_2);
			section3.Add (cell3_3);

			_groupRoot.Add (section1);
			_groupRoot.Add (section2);
			_groupRoot.Add (section3);

	}
	}
}

