﻿using System;
using UIKit;
using Easy.Core;
using CoreGraphics;
using Easy.Core.iOS;
using Easy.Core.Bind;

namespace Easy.Test.iOS
{
	public class FirstView : UIViewController, IReturnWithResult
	{

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = UIColor.White;

			var buttonPush = new UIButton (UIButtonType.RoundedRect);
			buttonPush.SetTitle ("Push", UIControlState.Normal);
			buttonPush.Frame = new CGRect (100, 120, View.Bounds.Width, 50);

			var buttonPresent = new UIButton (UIButtonType.RoundedRect);
			buttonPresent.SetTitle ("Present", UIControlState.Normal);
			buttonPresent.Frame = new CGRect (100, 200, View.Bounds.Width, 50);
			var textView = new UITextView (CGRect.Empty);
			var firstvm = new FirstVm ();
			var pushCommand = new ControlCommand (buttonPush, Events.UIButton_TouchUpInside, firstvm.PushCommand);
			var presentCommand = new ControlCommand (buttonPresent, Events.UIButton_TouchUpInside, firstvm.PresentCommand);

			View.AddSubviews (new[]{buttonPush,buttonPresent });
		}

		#region IReturnWithResult implementation

		public bool ReturnWithResult (Intent intent)
		{
			var data = intent.Get<Data> ("data");
			Console.WriteLine (String.Format ("Result: {0}",data == null ? string.Empty : data.Name));
			return true;
		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
		}

		public override void ViewDidDisappear (bool animated)
		{
			base.ViewDidDisappear (animated);
		}

		#endregion
	}
}

