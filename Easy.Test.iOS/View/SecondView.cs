﻿using System;
using UIKit;
using Easy.Core.iOS;
using CoreGraphics;
using Easy.Core;
using Easy.Core.Bind;

namespace Easy.Test.iOS
{
	[Present]
	public class SecondView : ViewControllerBase<SecondVm>
	{
		public SecondView ()
		{
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			base.ViewDidLoad ();
			View.BackgroundColor = UIColor.DarkGray;
			var buttonBack = new UIButton (UIButtonType.RoundedRect);
			buttonBack.SetTitle ("GoBack", UIControlState.Normal);
			buttonBack.Frame = new CGRect (100, 120, View.Bounds.Width, 50);

			var buttonWithResult = new UIButton (UIButtonType.RoundedRect);
			buttonWithResult.SetTitle ("GoBackWithResult", UIControlState.Normal);
			buttonWithResult.Frame = new CGRect (100, 200, View.Bounds.Width, 50);

			var pushCommand = new ControlCommand (buttonBack, Events.UIButton_TouchUpInside, DataContext.GoBackCommand);
			var presentCommand = new ControlCommand (buttonWithResult, Events.UIButton_TouchUpInside, DataContext.GoBackWithResultCommand);
			View.AddSubviews (new[]{buttonBack,buttonWithResult });
			Console.WriteLine ("Context: {0}", Context);
		}

	}
}

