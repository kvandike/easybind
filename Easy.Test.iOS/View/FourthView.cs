﻿using System;
using Easy.Core.iOS;
using UIKit;
using CoreGraphics;
using Easy.Core;

namespace Easy.Test.iOS
{
	public class FourthView : ViewControllerBase<FourthVm>
	{
		UITableView table;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			table = new UITableView(CGRect.Empty,UITableViewStyle.Grouped);

			var backButton = new UIBarButtonItem (UIBarButtonSystemItem.Cancel,(s,e)=>{
				var intent = DataContext.CreateIntent ();
				intent.Put ("four","hello, I'm four");
				intent.GoBackWithResult ();
			});
			NavigationItem.LeftBarButtonItem = backButton;

			var sourceGroup = new GroupTableSource (table, DataContext.GroupRoot);
			sourceGroup.CreateCell = (tableView, item, index) => {
				var cell = tableView.DequeueReusableCell (item.Tag ?? item.ToString ()) as CustomCell;
				if (cell == null) {
					cell = new CustomCell ();
				}
				var data = item.Data as Data;
				if (data != null) {
					cell.Text = data.Name;
				}
				return cell;
			};
			table.Source = sourceGroup;
			View.AddSubview (table);
		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
			table.Frame = View.Bounds;
		}

	}
}

