﻿using System;
using UIKit;
using CoreGraphics;
using Easy.Core;
using Easy.Core.iOS;
using Easy.Core.Bind;

namespace Easy.Test.iOS
{
	public class ThirdView : ViewControllerBase<ThirdVm>
	{
		public ThirdView (bool first, string text,int number)
		{
		}



		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = UIColor.Yellow;
			var buttonBack = new UIButton (UIButtonType.RoundedRect);
			buttonBack.SetTitle ("GoBack", UIControlState.Normal);
			buttonBack.Frame = new CGRect (100, 120, View.Bounds.Width, 50);

			var buttonWithResult = new UIButton (UIButtonType.RoundedRect);
			buttonWithResult.SetTitle ("GoBackWithResult", UIControlState.Normal);
			buttonWithResult.Frame = new CGRect (100, 200, View.Bounds.Width, 50);

			var buttonPushGroupTable = new UIButton (UIButtonType.RoundedRect);
			buttonPushGroupTable.SetTitle ("Push group Table", UIControlState.Normal);
			buttonPushGroupTable.Frame = new CGRect (100, 280, View.Bounds.Width, 50);

			var pushCommand = new ControlCommand (buttonBack, Events.UIButton_TouchUpInside, DataContext.GoBackCommand);
			var presentCommand = new ControlCommand (buttonWithResult, Events.UIButton_TouchUpInside, DataContext.GoBackWithResultCommand);
			var fourthCommand = new ControlCommand (buttonPushGroupTable, Events.UIButton_TouchUpInside, DataContext.PushGroupCommand);

			View.AddSubviews (new[]{buttonBack,buttonWithResult,buttonPushGroupTable });

			Console.WriteLine ("Context: {0}", Context);

		}

		#region IReturnWithResult implementation

		public override bool ReturnWithResult (Intent intent)
		{
			var data = intent.Get<string> ("four");
			Console.WriteLine (string.Format ("from four {0}",data));
			return true;
		}

		#endregion
	}
}

