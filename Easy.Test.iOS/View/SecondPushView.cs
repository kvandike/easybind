﻿using System;
using UIKit;
using Easy.Core.iOS;

namespace Easy.Test.iOS
{
	[Push]
	public class SecondPushView : UIViewController
	{
		public SecondPushView ()
		{
		}


		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			View.BackgroundColor = UIColor.White;
		}
	}
}

