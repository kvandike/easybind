﻿using System;
using UIKit;
using CoreGraphics;
using Easy.Core.iOS;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Easy.Core;
using Easy.Core.Bind;

namespace Easy.Test.iOS
{
	public class TableViewController : UIViewController
	{
		public TableViewController ()
		{
		}

		UITableView tableView;
		ObservableTableSource<Data> tableSource;
		ObservableCollection<Data> randoms;
		string CellIdentifier = "TableCell";


		UITextField oldFiled;
		UITextField newField;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			tableView = new UITableView (CGRect.Empty, UITableViewStyle.Plain);
			tableSource = new ObservableTableSource<Data> (tableView);
			tableSource.DataSource = Randoms;
			tableSource.GetHeightForHeaderDelegate += (tableview, section) => 120;
			tableSource.GetViewForHeaderDelegate += (tableview, section) => HeaderView;

			tableSource.BindCellDelegate += (tableView, index, item) => {
				UITableViewCell cell = tableView.DequeueReusableCell (CellIdentifier) ?? new UITableViewCell (UITableViewCellStyle.Default, CellIdentifier);
				cell.TextLabel.Text = item.Name;
				return cell;
			};
			tableView.Source = tableSource;
			View.AddSubview (tableView);
		}


		UIView HeaderView{
			get{
				var view = new UIView (new CGRect(0,0,View.Bounds.Width,120));
				view.BackgroundColor = UIColor.White;
				var addbutton = new UIButton (UIButtonType.RoundedRect);
				addbutton.Frame = new CGRect (10, 5, 60, 30);



				addbutton.SetTitle ("Add", UIControlState.Normal);
				var movebutton = new UIButton (UIButtonType.RoundedRect);
				movebutton.Frame = new CGRect (10, 35, 60, 30);
				movebutton.SetTitle ("Move", UIControlState.Normal);
				var removebutton = new UIButton (UIButtonType.RoundedRect);

				oldFiled = new UITextField(new CGRect(110,35,30,30));
				oldFiled.Text="0";
				oldFiled.BackgroundColor = UIColor.LightTextColor;
				newField = new UITextField(new CGRect(150,35,30,30));
				newField.BackgroundColor = UIColor.LightTextColor;
				newField.Text="0";

				removebutton.Frame = new CGRect (10, 65, 60, 30);
				removebutton.SetTitle ("Remove", UIControlState.Normal);
				view.AddSubviews (new  UIView[]{ addbutton, movebutton,oldFiled,newField, removebutton });

				var addcommand = new ControlCommand (addbutton, Events.UIButton_TouchUpInside, AddCommand);
				var movecommand = new ControlCommand (movebutton,  Events.UIButton_TouchUpInside, MoveCommand);
				var removecommand = new ControlCommand (removebutton,  Events.UIButton_TouchUpInside, RemoveCommand);

				return view;
			}
		}

		public override void ViewDidLayoutSubviews ()
		{
			base.ViewDidLayoutSubviews ();
			tableView.Frame = View.Bounds;
		}


		ICommand AddCommand{
			get{
				return new RelayCommand (x => {
					var count = Randoms.Count;
					Randoms.Insert(0,new Data{Name =  count.ToString()});
				});
			}
		}

		ICommand MoveCommand{
			get{
				return new RelayCommand (x => {
					var oldF = int.Parse(oldFiled.Text);
					var newF = int.Parse(newField.Text);
					Randoms.Move(oldF,newF);
				});
			}
		}

		ICommand RemoveCommand{
			get{
				return new RelayCommand (x => {
					var count = Randoms.Count;
					Randoms.RemoveAt(0);
				});
			}
		}

		ObservableCollection<Data> Randoms{
			get{
				if (randoms == null) {
					randoms = new ObservableCollection<Data> ();
					for (int i = 0; i < 20; i++) {
						randoms.Add (new Data{Name = i.ToString ()});
					}
				}
				return randoms;
			}
		}
	}
}

