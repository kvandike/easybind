﻿using System.Reflection;

namespace Easy.Core.Bind
{
	class Trigger : TriggerBase {
		public MemberInfo Member;

		internal Trigger(object target, MemberInfo member):base(target){
			Member = member;
		}

		public object GetValue(){
			return Extension.GetValue (Target, Member); 
		}

		public bool SetValue(object value){
			return Extension.SetValue (Target, Member, value);
		}
	}
}

