﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Easy.Core.Bind
{
	public class BindingBuilder
	{
		bool isSourcePropertyChangeEnable = true;
		bool isTargetPropertyChangeEnable = true;
		BindingMode bindingMode= BindingMode.OneWay;
		string customSourceEventName;
		string customTargetEventName;
		Expression sourcePropertyExpression;
		Expression targetPropertyExpression;
		object sourceRoot;
		object targetRoot;


		public static BindingBuilder Create<TSource,TTarget>(object source, Expression<Func<TSource>> sourceExpression, object target, Expression<Func<TTarget>> targetExpression){
			var builder = new BindingBuilder ();
			builder
				.SetSource (source, sourceExpression)
				.SetTarget (target, targetExpression);
			return builder;
		}

		public BindingBuilder SetSource<TSource>(object source, Expression<Func<TSource>> sourceExpression){
			sourceRoot = source;
			sourcePropertyExpression = sourceExpression;
			return this;
		}

		public BindingBuilder SetTarget<TTarget>(object target, Expression<Func<TTarget>> targetExpression){
			targetRoot = target;
			targetPropertyExpression = targetExpression;
			return this;
		}


		public BindingBuilder SetTarget<TTarget>(Expression<Func<TTarget>> targetExpression){
			var member = targetExpression.Body as MemberExpression;
			if (member == null) {
				throw new ArgumentException ("targetExpression is not memberExpression");
			}
			targetPropertyExpression = targetExpression;
			return SetTarget (Extension.EvalTarget (member), targetExpression);
		}

		public BindingBuilder SetBinding(BindingMode mode){
			bindingMode = mode;
			return this;
		}

		public BindingBuilder WithSourcePropertyChange(bool enable){
			isSourcePropertyChangeEnable = enable;
			return this;
		}

		public BindingBuilder WithTargetPropertyChange(bool enable){
			isTargetPropertyChangeEnable = enable;
			return this;
		}

		public BindingBuilder UpdateSourceTrigger(string eventName){
			customSourceEventName = eventName;
			return this;
		}

		public BindingBuilder UpdateTargetTrigger(string eventName){
			customTargetEventName = eventName;
			return this;
		}
			

		public Binding Build(){
			if (targetRoot == null || sourceRoot == null) {
				throw new ArgumentException ("targetRoot or sourceRoot are null");
			}
			if (targetPropertyExpression == null || sourcePropertyExpression == null) {
				throw new ArgumentException ("targetPropertyExpression or sourcePropertyExpression are null");
			}
			var sourceMember = Extension.GetMemberInfo (sourcePropertyExpression);
			var sourceTrigger = new Trigger (sourceRoot, sourceMember);

			var targetMember = Extension.GetMemberInfo (targetPropertyExpression);
			var targetTrigger = new Trigger (targetRoot, targetMember);

			var binding = new Binding (sourceTrigger, targetTrigger);
			binding.SubscribeSourceFromChangeNotificationEvent (customSourceEventName, isSourcePropertyChangeEnable);
			if (bindingMode == BindingMode.TwoWay) {
				binding.SubscribeTargetFromChangeNotificationEvent (customTargetEventName, isTargetPropertyChangeEnable);
			}
			return binding;
		}


	


	}
}

