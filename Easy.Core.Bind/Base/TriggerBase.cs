﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Easy.Core.Bind
{
	public abstract class TriggerBase 
	{
		public object Target;
		IDictionary<string,Tuple<EventInfo,Delegate>> events;

		protected IDictionary<string, Tuple<EventInfo, Delegate>> Events {
			get {
				return events ?? (events = new Dictionary<string,Tuple<EventInfo,Delegate>> ());
			}
		}

		protected TriggerBase(object target){
			Target = target;
		}

		#region ISubscribe implementation

		public void Subscribe (string eventName, EventHandler handler)
		{
			var type = Target.GetType ();
			Delegate eventHandler;
			EventInfo ev = Extension.GetEvent (type, eventName);
			if (ev != null) {
				var isClassicHandler = typeof(EventHandler).GetTypeInfo ().IsAssignableFrom (ev.EventHandlerType.GetTypeInfo ());
				eventHandler = isClassicHandler ? handler : Extension.CreateGenericEventHandler (ev, () => handler (null, EventArgs.Empty));
				ev.AddEventHandler (Target, eventHandler);
				Events.Add (eventName, new Tuple<EventInfo, Delegate> (ev, eventHandler));
			}
		}
		public void UnSubscribe ()
		{
			foreach (var item in Events) {
				item.Value.Item1.RemoveEventHandler (Target, item.Value.Item2);
			}
		}

		#endregion
	

	}
}

